# README #

This is a repository of Moodle SQL queries for creating reports. If you wish to contribute, fork the repo and make a pull request.

### Contribution guidelines ###

* All table names should be prefixed with mdl_.  It's assumed most Moodle installs have this. If yours is different, you'll have to make those changes locally, not here.
* Just SQL, no code.
* Only add queries you have the right to add. If you've copied it from somewhere else, don't add it without permission. If there's another source of queries, I'll add a link to it in the WIKI.
* If you have an idea for a report, but don't know how to write SQL, raise an issue, and perhaps someone will volunteer to write it.